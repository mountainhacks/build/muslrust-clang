# Build image for static binary

This image is a helper for building static Rust binaries with Musl. The image also
offers to link with Clang instead of GCC.

## Usage

```docker
FROM registry.gitlab.com/mountainhacks/build/muslrust-clang:1.75.0 as builder

COPY . .

RUN cargo build --release --bin mybinary

FROM alpine:latest

COPY --from=builder /build/target/x86_64-unknown-linux-musl/release/mybinary
```
