FROM ubuntu:mantic as build

# Non-interactive.
ENV DEBIAN_FRONTEND="noninteractive"

# Install LLVM and Clang
RUN apt-get update && apt-get install --quiet --yes --no-install-recommends \
  clang-17 \
  ca-certificates \
  autoconf \
  automake \
  cmake \
  curl \
  file \
  git \
  libtool \
  lld-17 \
  llvm-17 \
  musl-dev \
  musl-tools \
  make \
  && rm -rf /var/lib/apt/lists/* \
  && rm -rf /var/cache/apt/archives/*

# Note: this is a workarround for fixing missing musl tools in muslrust.
RUN ln -s /usr/bin/x86_64-linux-gnu-gcc-ar /usr/bin/musl-ar \
  && ln -s /usr/bin/x86_64-linux-gnu-gcc-nm /usr/bin/musl-nm \
  && ln -s /usr/bin/x86_64-linux-gnu-gcc-ranlib /usr/bin/musl-ranlib

ARG RUST_VERSION="stable"

ENV RUSTUP_VERSION="1.26.0" \
  RUSTUP_ARCH="x86_64-unknown-linux-gnu"

RUN curl "https://static.rust-lang.org/rustup/archive/${RUSTUP_VERSION}/${RUSTUP_ARCH}/rustup-init" -o rustup-init \
  && chmod +x rustup-init \
  && ./rustup-init -y --default-toolchain "${RUST_VERSION}" --profile minimal --no-modify-path \
  && rm rustup-init \
  && ~/.cargo/bin/rustup target add x86_64-unknown-linux-musl

# Grant access to non-root to cargo.
RUN chmod a+X /root

# Squash layers.
FROM scratch

ENV PATH=/root/.cargo/bin:${PATH} \
  RUSTUP_HOME=/root/.rustup \
  CARGO_BUILD_TARGET=x86_64-unknown-linux-musl \
  DEBIAN_FRONTEND=noninteractive \
  TZ=Etc/UTC

COPY --from=build / /

WORKDIR /build
